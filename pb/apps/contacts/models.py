import os
import Image

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.files.images import ImageFile

from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):
    title = models.CharField(_('Parent'), max_length=250, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    class MPTTMeta:
        order_insertion_by = ('title',)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return '/?category=%d' % self.pk


class Phone(models.Model):
    category = models.ForeignKey(Category, related_name='phones')
    number = models.PositiveIntegerField(_('Number'), unique=True)
    details = models.CharField(_('Details'), max_length=1000, null=True, blank=True)
    room = models.CharField(_('Room'), max_length=50)

    class Meta:
        ordering = ('number',)
        verbose_name = _('Phone')
        verbose_name_plural = _('Phones')

    def __unicode__(self):
        return unicode(self.number)


class Man(models.Model):
    phones = models.ManyToManyField('Phone', through='ManPhone', related_name='users')
    lname = models.CharField(_('Last name'), max_length=100)
    fname = models.CharField(_('First name'), max_length=100)
    mname = models.CharField(_('Middle name'), max_length=100, blank=True, null=True)
    photo = models.ImageField(_('Photo'), upload_to='photos', blank=True, null=True)
    position = models.CharField(_('Position'), max_length=500, blank=True, null=True)

    class Meta:
        ordering = ('lname', 'fname',)
        verbose_name = _('Man')
        verbose_name_plural = _('People')

    def __unicode__(self):
        return settings.NAME_PATTERN.format(
                    fname=self.fname,
                    mname=self.mname or '',
                    lname=self.lname).strip()

    def save(self, *args, **kwargs):
        miniature = kwargs.pop('miniature', True)
        super(Man, self).save(*args, **kwargs)
        if miniature and self.photo:
            filepath = self.photo.path
            size = settings.PHOTO_SIZE
            identifier = '_%d.' % size
            if filepath.find(identifier) == -1:
                image = Image.open(filepath)
                image.thumbnail([size, size], Image.ANTIALIAS)
                new_filepath = filepath.split('.')
                new_filepath = '.'.join(new_filepath[:-1]) + identifier + new_filepath[-1].lower()
                try:
                    image.save(new_filepath, image.format, quality=90, optimize=1)
                except:
                    image.save(new_filepath, image.format, quality=90)
                # use resized image for photo
                photo_name = self.photo.name.split('.')
                photo_name = '.'.join(photo_name[:-1]) + identifier + photo_name[-1].lower()
                self.photo = photo_name
                self.save(miniature=False)
                os.remove(filepath)


class ManPhone(models.Model):
    man = models.ForeignKey(Man, related_name='man_phones')
    phone = models.ForeignKey(Phone, related_name='phone_users')
    description = models.CharField(_('Description'), max_length=500, blank=True, null=True)

    def __unicode__(self):
        return u'%s > %s' % (self.man.__unicode__(), self.phone.__unicode__())

    class Meta:
        verbose_name = _('ManPhone')
        verbose_name_plural = _('ManPhones')
        unique_together = ('man', 'phone')
