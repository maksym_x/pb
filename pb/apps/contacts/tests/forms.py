from django.test import TestCase
from django.db.models import Q

from ..forms import CategoryForm, SearchForm, ManDetailsForm

from .factories import CategoryFactory, ManFactory


class FormsTestCase(TestCase):
    
    def test_category_form(self):
        category = CategoryFactory.create()
        test_data = [
            {
                'category': category.pk,
                'is_valid': True},
            {
                'category': str(category.pk),
                'is_valid': True},
            {
                'category': category.pk + 10000,
                'is_valid': False},
            {
                'is_valid': False}]
        for data in test_data:
            form = CategoryForm(data)
            self.assertEqual(form.is_valid(), data['is_valid'])

        form = CategoryForm(test_data[0])
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['category'].pk, category.pk)

    def test_search_form(self):
        test_data = [
            {
                'is_valid': False,},
            {
                'q': 'some text',
                'is_valid': True,
                'result': "(OR: (AND: ), ('users__lname__icontains', u'some'), "
                "('users__fname__icontains', u'some'), ('users__position__icontains', u'some'), "
                "('category__title__icontains', u'some'), ('room__icontains', u'some'), "
                "('users__lname__icontains', u'text'), ('users__fname__icontains', u'text'), "
                "('users__position__icontains', u'text'), ('category__title__icontains', u'text'), "
                "('room__icontains', u'text'))"},
            {
                'q': 'some 1234',
                'is_valid': True,
                'result': "(OR: (AND: ), ('users__lname__icontains', u'some'), "
                "('users__fname__icontains', u'some'), ('users__position__icontains', u'some'), "
                "('category__title__icontains', u'some'), ('room__icontains', u'some'), "
                "('number', 1234), ('room', '1234'))"}]

        for data in test_data:
            form = SearchForm(data)
            is_valid = form.is_valid()
            self.assertEqual(is_valid, data['is_valid'])
            if is_valid:
                self.assertEqual(str(form.cleaned_data['q']), data['result'])

    def test_man_details(self):
        man = ManFactory.create()
        test_data = [{
            'pk': man.pk,
            'is_valid': True,
        }, {
            'pk': str(man.pk),
            'is_valid': True,
        }, {
            'is_valid': False,
        }, {
            'pk': -10,
            'is_valid': False
        }, {
            'pk': man.pk + 1000,
            'is_valid': False,
        }]

        for data in test_data:
            form = ManDetailsForm(data)
            self.assertEqual(form.is_valid(), data['is_valid'])

        form = ManDetailsForm(test_data[0])
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['man'].pk, man.pk)
