from django.test import TestCase
from django.core.urlresolvers import reverse

from ..models import Phone

from .factories import CategoryFactory, PhoneFactory, ManFactory


class ViewsTestCase(TestCase):

    def test_index_pagination(self):
        category = CategoryFactory.create()
        per_page = 5
        for i in range(per_page * 2):
            PhoneFactory.create(category=category)
        phones = Phone.objects.all()
        self.assertEqual(phones.count(), per_page * 2)
        with self.settings(DEFAULT_ITEMS_PER_PAGE=per_page):
            r = self.client.get(
                            reverse('index'),
                            {'page': 1, 'category': category.pk})
            self.assertEqual(r.status_code, 200)
            self.assertContains(r, phones[per_page - 2].number)
            self.assertNotContains(r, phones[per_page * 2 - 2].number)
            r = self.client.get(
                            reverse('index'),
                            {'page': 2, 'category': category.pk})
            self.assertContains(r, phones[per_page * 2 - 2].number)
            # check man details url
            self.assertContains(r, reverse('details'))

    def test_search(self):
        category = CategoryFactory.create()
        phone1 = PhoneFactory.create(category=category)
        phone2 = PhoneFactory.create(category=category)
        man1 = ManFactory.create(phone=phone1)
        man2 = ManFactory.create(phone=phone2)
        man3 = ManFactory.create(phone=phone2)
        with self.settings(DEFAULT_ITEMS_PER_PAGE=10):
            r = self.client.get(
                            reverse('index'),
                            {'q': str(phone1.number)})
            self.assertEqual(r.status_code, 200)
            self.assertContains(r, phone1.room)
            self.assertContains(r, man1.lname)
            self.assertNotContains(r, phone2.room)
            self.assertNotContains(r, man2.lname)
            r = self.client.get(
                            reverse('index'),
                            {'q': man2.lname})
            self.assertContains(r, phone2.number)
            self.assertNotContains(r, phone1.number)
            # phone and lname specified
            r = self.client.get(
                            reverse('index'),
                            {'q': '%s %d' % (man3.lname, phone1.number)})
            self.assertContains(r, phone1.number)
            self.assertContains(r, phone2.number)

    def test_details(self):
        r = self.client.get(reverse('details'))
        self.assertEqual(r.status_code, 404)
        phone = PhoneFactory.create()
        man = ManFactory.create(phone=phone)
        r = self.client.get(
                        reverse('details'),
                        {'pk': man.pk},
                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(r.status_code, 200)
        self.assertContains(r, unicode(man))
        self.assertContains(r, phone.number)
