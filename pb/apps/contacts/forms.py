from django import forms
from django.db.models import Q

from .models import Category, Man


class CategoryForm(forms.Form):
    category = forms.IntegerField(min_value=1)

    def clean_category(self):
        category_pk = self.cleaned_data.get('category')
        try:
            category = Category.objects.get(pk=category_pk)
        except Category.DoesNotExist:
            raise forms.ValidationError('Category with specified id does not exists!')
        return category


class SearchForm(forms.Form):
    """
    returns django.db.models.Q object

    :param q: search query
    """

    q = forms.CharField()

    def phone_number(self, text):
        """
        if text - phone number - return number
        if not - return None

        :param: text - word in search query
        """
        allowed_chars = ('0', '1', '2', '3', '4', '5', '6', '7', '8' , '9', '-')
        for i in text:
            if i not in allowed_chars:
                return None
        return int(text.replace('-', ''))

    def clean(self):
        data = self.cleaned_data
        query = data.get('q')
        q = Q()
        if not query:
            data['q'] = q
            return data
        parts = query.split(' ')
        for part in parts:
            number = self.phone_number(part)
            if number:
                q |= Q(number=number) | Q(room=str(number))
            else:
                q |= (
                    Q(users__lname__icontains=part) |
                    Q(users__fname__icontains=part) |
                    Q(users__position__icontains=part) |
                    Q(category__title__icontains=part) |
                    Q(room__icontains=part))
        data['q'] = q
        return data


class ManDetailsForm(forms.Form):
    """
    Raise exception if man with specified pk does not exists.
    If man exists, returns Man object.
    """
    pk = forms.IntegerField(min_value=0)

    def clean(self):
        data = self.cleaned_data
        try:
            man = Man.objects.get(pk=data['pk'])
        except KeyError:
            raise forms.ValidationError('Man with specified pk does not exists!')
        except Man.DoesNotExist:
            raise forms.ValidationError('Man with specified pk does not exists!')
        data['man'] = man
        return data
