from django.core.urlresolvers import reverse


class AdminURLs:

    urls = {}

    @classmethod
    def url(self, obj, action='change', id=None):
        """
        :param: `obj` - model class or object
        :param: `action` - [add|change]
        :param: `id` - id of object to change
        """
        table = obj._meta.db_table
        key = '%s&%s' % (table, action)
        if key in self.urls:
            url = self.urls[key]
        else:
            if hasattr(obj._meta, 'model'):
                url = reverse('admin:%s_%s_%s' % (
                        obj._meta.app_label,
                        obj._meta.model,
                        action.replace('change', 'changelist')))
            else:
                url = reverse('admin:%s_%s_%s' % (
                        obj._meta.app_label,
                        obj._meta.module_name,
                        action.replace('change', 'changelist')))
            self.urls[key] = url
        if id:
            url = '%s%s/' % (url, id)
        return url
