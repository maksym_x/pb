from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

from pb.settings.utils import rel


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', 'pb.apps.contacts.views.index', name='index'),
    url(r'^contacts/', include('pb.apps.contacts.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$',
                'django.views.static.serve',
                {'document_root': rel('media')}
        ),
    )
