PIPELINE_CSS = {
    'bootstrap': {
        'source_filenames': (
            'css/bootstrap.less',
        ),
        'output_filename': 'css/bootstrap.min.css',
    },
    'pb': {
        'source_filenames': (
            'css/pb.less',
        ),
        'output_filename': 'css/pb.min.css',
    },
}

PIPELINE_JS = {
    'jquery': {
        'source_filenames': (
            'js/libs/jquery.js',
        ),
        'output_filename': 'js/libs/jquery.min.js',
    },
    'pb': {
        'source_filenames': (
            'js/libs/URI.min.js',
            'js/libs/pagination.js',
            'js/pb.js',
        ),
        'output_filename': 'js/pb.min.js',
    },
}

# When PIPELINE is True, CSS and JavaScripts will be concatenated and filtered.
# When False, the source-files will be used instead.
# Default: PIPELINE = not DEBUG

PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yui.YUICompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yui.YUICompressor'

PIPELINE_YUI_BINARY = '/usr/bin/yui-compressor'

PIPELINE_COMPILERS = (
  'pb.apps.core.compilers.RubyLesscCompiler',
)

PIPELINE_LESS_BINARY = '/usr/local/bin/lessc'

# PIPELINE_DISABLE_WRAPPER = True
